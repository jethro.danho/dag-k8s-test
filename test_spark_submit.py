
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta
from airflow.utils.dates import days_ago

default_args = {
    "owner": "Jethro",
    "depends_on_past": False,
    "start_date": days_ago(1),
    "email": ["cyrille1.danho@orange.com"],
    "email_on_failure": True,
    "email_on_retry": False,
    "retries": 1,
    "retry_delay": timedelta(minutes=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
}

dag = DAG("test_spark", default_args=default_args, schedule_interval=timedelta(1))

# print_date, sleep and spark_submit are examples of tasks created by instantiating operators
print_date = BashOperator(task_id="print_date", bash_command="date", dag=dag)

sleep = BashOperator(task_id="sleep", bash_command="sleep 5", retries=3, dag=dag)

spark_command = """/opt/airflow/spark/bin/spark-submit --master k8s://https://rancher-dev.ocitnetad.ci/k8s/clusters/local --deploy-mode cluster --name spark-pi --class org.apache.spark.examples.SparkPi --conf spark.kubernetes.authenticate.driver.serviceAccountName=spark --conf spark.executor.instances=5 --conf spark.kubernetes.container.image=apache/spark local:///opt/spark/examples/jars/spark-examples_2.12-3.4.1.jar"""

spark_submit = BashOperator(
    task_id="spark_submit",
    bash_command=spark_command,
    #retry = 2,
    dag=dag,
)

sleep.set_upstream(print_date)
spark_submit.set_upstream(print_date)


