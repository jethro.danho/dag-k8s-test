from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.contrib.operators.spark_submit_operator import SparkSubmitOperator

from datetime import datetime, timedelta
from airflow.utils.dates import days_ago

###############################################
# Parameters for spark config
###############################################
spark_master = "k8s://192.168.49.2:8443"
spark_app_name = "Spark-pi"


_config ={
    # "spark.master":spark_master,
    'deploy-mode' : 'cluster',
    'spark.kubernetes.authenticate.driver.serviceAccountName': 'default',
    'spark.executor.instances':3,
    'spark.kubernetes.driver.container.image': 'sparkpy:latest',
    'spark.kubernetes.executor.container.image': 'sparkpy:latest',
}


###############################################
# DAG Definition
###############################################


default_args = {
    "owner": "Jethro",
    "depends_on_past": False,
    "start_date": days_ago(1),
    "email": ["jethro.danho@data354.co"],
    "email_on_failure": True,
    "email_on_retry": False,
    "retries": 1,
    "retry_delay": timedelta(minutes=5)
}

dag = DAG(
        "spark-job", 
        default_args=default_args, 
        schedule_interval=timedelta(days=1)
    )

start = BashOperator(task_id="print_date", bash_command="date", dag=dag)

spark_submit = SparkSubmitOperator(
    task_id="spark_submit",
    application="/opt/spark/examples/src/main/python/pi.py",
    name=spark_app_name,
    conn_id="spark_k8s",
    verbose=0,
    conf=_config,
    dag=dag
)


end = BashOperator(task_id="sleep", bash_command="sleep 5", retries=3, dag=dag)

start >> spark_submit  >> end
